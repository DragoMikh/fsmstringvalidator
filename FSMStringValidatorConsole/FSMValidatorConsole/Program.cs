﻿internal class Program
{
    private static void Main(string[] args)
    {
        string validator =
@"
start->inter #
start->inte2 #
start->end+ \c
inter->end \d
";

        var fsm = FSMStringValidator.FSMCompiler.compile(validator);
        var r1 = fsm.Validate("_a");
        var r2 = fsm.Validate("a1");
        var r3 = fsm.Validate("a1bc");
    }
}