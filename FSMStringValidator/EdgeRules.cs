﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSMStringValidator
{
    public class MatchAll : EdgeRule
    {
        public MatchAll():base()
        {
        }

        public override bool Satisfies(char c)
        {
            return true;
        }
    }
    public class MatchCharacter : EdgeRule
    {
        public MatchCharacter():base()
        {
        }

        public override bool Satisfies(char c)
        {
            return char.IsLetter(c);
        }
    }
    public class MatchDigit : EdgeRule
    {
        public MatchDigit():base()
        {
        }

        public override bool Satisfies(char c)
        {
            return char.IsDigit(c);
        }
    }
    public class MatchCharacterRange : EdgeRule
    {
        char _start;
        char _end;
        public MatchCharacterRange(char start, char end)
        {
            _start = start;
            _end = end;
        }

        public override bool Satisfies(char c)
        {
            return c >= _start && c <= _end;
        }
    }
    public class MatchDistinctCharacter : EdgeRule
    {
        char _character;
        public MatchDistinctCharacter(char character) 
        { 
            _character = character;
        }
        public override bool Satisfies(char c)
        {
            return _character == c;
        }
    }
}
