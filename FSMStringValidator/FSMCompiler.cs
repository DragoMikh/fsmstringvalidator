﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FSMStringValidator
{
    public class FSMCompiler
    {
        /*
         * Define Nodes and Edges of the FSM in the Following Syntax
         * [StartNode]->[EndNode][+][Rule]
         * StartNode = 
         * + = Is Node Valid End Node
         * Rule = Rule a character has to pass to follow this edge
         */

        public static FSM compile(string description)
        {
            Dictionary<string, FSMNode> nodes = new Dictionary<string, FSMNode>();
            List<FSMEdge> edges = new List<FSMEdge>();

            string[] rules = description.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
            foreach (string rule in rules)
                parseLine(rule, nodes, edges);

            return new FSM(nodes.First().Value, nodes.Select(n => n.Value).ToList());
        }
        private static void parseLine(string line, Dictionary<string, FSMNode> nodes, List<FSMEdge> edges)
        {
            string[] parts = line.Split(' ');
            string[] lnodes = parts.First().Split("->");

            string startNodeName = lnodes[0].Trim('+');
            bool startIsValidEndNode = lnodes[0].EndsWith('+');

            string endNodeName = lnodes[1].Trim('+');
            bool endIsValidEndNode = lnodes[1].EndsWith("+");

            FSMNode startNode = null;
            FSMNode endNode = null;

            if (!nodes.TryGetValue(startNodeName, out startNode))
            {
                startNode = new FSMNode();
                nodes.Add(startNodeName, startNode);
            }
            if (!nodes.TryGetValue(endNodeName, out endNode))
            {
                endNode = new FSMNode();
                nodes.Add(endNodeName, endNode);
            }

            if (startIsValidEndNode)
                startNode.Valid = true;

            if (endIsValidEndNode)
                endNode.Valid = true;

            var rule = parseRule(parts[1]);
            FSMEdge edge = new FSMEdge(rule, endNode);
            startNode.Outgoing.Add(edge);
            edges.Add(edge);
        }
        private static EdgeRule parseRule(string rule)
        {
            switch(rule)
            {
                case "#":
                    return new MatchAll();

                case "\\c":
                    return new MatchCharacter();

                case "\\d":
                    return new MatchDigit();
            }

            if(rule.Contains("-"))
            {
                char start;
                char end;
                StringReader str = new StringReader(rule);                
                do
                {
                    start = (char)str.Read();
                    str.Read();
                    end = (char)str.Read();
                }
                while (str.Peek() != -1);

                return new MatchCharacterRange()
            }

            return null;
        }
    }
}
