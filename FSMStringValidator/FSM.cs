﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FSMStringValidator
{
    /// <summary>
    /// A Finite State Machine for Validating Strings
    /// </summary>
    public class FSM
    {
        //All Nodes in the FSM
        List<FSMNode> _nodes = new List<FSMNode>();
        //The Entry Node of the FSM
        FSMNode _start;

        internal FSM(FSMNode start, List<FSMNode> nodes)
        {
            _start = start;
            _nodes = nodes;
        }

        /// <summary>
        /// Check if a String is validated by the FSM
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool Validate(string str)
        {
            //Current Positions in the FSM Graph
            List<FSMNode> currentPosition = new List<FSMNode>() { _start };
            //Nodes of the next Iteration
            List<FSMNode> nextPositions = new List<FSMNode>();
            //
            foreach(char c in str)
            {
                for(int i = 0; i < currentPosition.Count; i++)
                {
                    foreach(var next in currentPosition[i].Outgoing)
                    {
                        if (next.Rule.Satisfies(c))
                            nextPositions.Add(next.Target);
                    }
                }
                currentPosition = nextPositions.ToList();
                nextPositions.Clear();

                if (currentPosition.Count == 0)
                    break;
            }

            return currentPosition.Any(n => n.Valid);
        }
    }

    internal class FSMNode
    {
        public bool Valid;
        public List<FSMEdge> Outgoing = new List<FSMEdge>();
    }
    internal class FSMEdge
    {
        public readonly EdgeRule Rule;
        public readonly FSMNode Target;

        public FSMEdge(EdgeRule rule, FSMNode target)
        {
            Rule = rule;
            Target = target;
        }
        public FSMEdge(FSMNode target) :this(new MatchAll(), target)
        {
        }
    }
    public abstract class EdgeRule
    {
        public EdgeRule()
        {
        }

        public abstract bool Satisfies(char c);
    }
}
